# KARURA KUBERNETES CLUSTER STORAGE

## STORAGE WITH HEKETI AND GLUSTERFS

### Prerequisite
* Download heketic-cli ```https://github.com/heketi/heketi/releases/download/v7.0.0/heketi-client-v7.0.0.linux.amd64.tar.gz```
* Run on every host with glusterfs disk
```bash
iptables -N HEKETI
iptables -A HEKETI -p tcp -m state --state NEW -m tcp --dport 24007 -j ACCEPT
iptables -A HEKETI -p tcp -m state --state NEW -m tcp --dport 24008 -j ACCEPT
iptables -A HEKETI -p tcp -m state --state NEW -m tcp --dport 2222 -j ACCEPT
iptables -A HEKETI -p tcp -m state --state NEW -m multiport --dports 49152:49251 -j ACCEPT
service iptables save
```

### Setup
* Run on glusterfs nodes
```bash
modprobe dm_thin_pool
```
* kubectl apply -f heketi/glusterfs-daemonset.json -n storage
* kubectl label node <...node...> storagenode=glusterfs
* Install on glusterfs nodes
```bash
apt install -y glusterfs-client 
``` 
* kubectl apply -f heketi/heketi-service-account.json -n storage
* kubectl create clusterrolebinding heketi-gluster-admin --clusterrole=edit --serviceaccount=storage:heketi-service-account -n storage
* kubectl create secret generic heketi-config-secret --from-file=./heketi/heketi.json -n storage
* kubectl apply -f heketi/heketi-bootstrap.json -n storage
* port-forward to heketi-server in cluster and set env 'export HEKETI_CLI_SERVER=http://localhost:$BIND_PORT'
* heketi-cli topology load --json=heketi/topology-sample.json
* heketi-cli setup-openshift-heketi-storage --replica 2
* kubectl apply -f heketi/heketi-storage.json -n storage
* kubectl delete all,service,jobs,deployment,secret --selector="deploy-heketi" -n storage
* kubectl apply -f heketi/heketi-deployment.json -n storage

### Test
* port-forward to heketi-server in cluster and set env 'export HEKETI_CLI_SERVER=http://localhost:$BIND_PORT'
* heketi-cli cluster list
* heketi-cli volume list

### Kubernetes Storage Class
* add nameserver 10.32.0.10 to /etc/resolv.conf
* kubectl apply -f glusterfs-storage-class.yaml
