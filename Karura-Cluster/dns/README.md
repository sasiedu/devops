## KUBERNETES DNS

### Kube-dns
```bash
kubectl apply -f kube-dns.yaml
```

### Core-dns
```bash
kubectl -n kube-system apply -f coredns.yaml
```

### For funny error (nslookup <public dnsname>)
```bash
Ref: https://github.com/kubernetes/kubernetes/issues/21613#issuecomment-343190401
modprobe br_netfilter
```
