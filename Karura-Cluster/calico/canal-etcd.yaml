# This ConfigMap can be used to configure a self-hosted Canal installation.
kind: ConfigMap
apiVersion: v1
metadata:
  name: canal-config
  namespace: kube-system
data:
  # Configure this with the location of your etcd cluster.
  etcd_endpoints: "https://172.168.0.5:2379,https://172.168.1.5:2379"

  # The interface used by canal for host <-> host communication.
  # If left blank, then the interface is chosing using the node's
  # default route.
  canal_iface: ""

  # Whether or not to masquerade traffic to destinations not within
  # the pod network.
  masquerade: "true"

  # The CNI network configuration to install on each node.  The special
  # values in this config will be automatically populated.
  cni_network_config: |-
    {
        "name": "canal",
        "cniVersion": "0.3.0",
        "plugins": [
            {
                "type": "flannel",
                "delegate": {
                    "type": "calico",
                    "etcd_endpoints": "https://172.168.0.5:2379,https://172.168.1.5:2379",
                    "etcd_key_file": "/etc/etcd/kubernetes-key.pem",
                    "etcd_cert_file": "/etc/etcd/kubernetes.pem",
                    "etcd_ca_cert_file": "/etc/etcd/ca.pem",
                    "log_level": "info",
                    "policy": {
                        "type": "k8s",
                        "k8s_api_root": "https://__KUBERNETES_SERVICE_HOST__:__KUBERNETES_SERVICE_PORT__",
                        "k8s_auth_token": "__SERVICEACCOUNT_TOKEN__"
                    },
                    "kubernetes": {
                        "kubeconfig": "/etc/cni/net.d/__KUBECONFIG_FILENAME__"
                    }
                }
            },
            {
                "type": "portmap",
                "capabilities": {"portMappings": true},
                "snat": true
            }
        ]
    }

  # If you're using TLS enabled etcd uncomment the following.
  # You must also populate the Secret below with these files.
  etcd_ca: "/etc/etcd/ca.pem"
  etcd_cert: "/etc/etcd/kubernetes.pem"
  etcd_key: "/etc/etcd/kubernetes-key.pem"

---
# The following contains k8s Secrets for use with a TLS enabled etcd cluster.
# For information on populating Secrets, see http://kubernetes.io/docs/user-guide/secrets/
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: calico-etcd-secrets
  namespace: kube-system
data:
  # Populate the following files with etcd TLS configuration if desired, but leave blank if
  # not using TLS for etcd.
  # This self-hosted install expects three files with the following names.  The values
  # should be base64 encoded strings of the entire contents of each file.
  etcd_ca: "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUYxakNDQTc2Z0F3SUJBZ0lVWnMwWklENWNvM0N2VzlPQXN6SzBNM3BHenIwd0RRWUpLb1pJaHZjTkFRRU4KQlFBd2NURUxNQWtHQTFVRUJoTUNXa0V4RURBT0JnTlZCQWdUQjBkaGRYUmxibWN4RlRBVEJnTlZCQWNUREVwdgphR0Z1Ym1WelluVnlaekVUTUJFR0ExVUVDaE1LUzNWaVpYSnVaWFJsY3pFUE1BMEdBMVVFQ3hNR1JHVjJiM0J6Ck1STXdFUVlEVlFRREV3cExkV0psY201bGRHVnpNQjRYRFRFNE1EY3lOakU1TlRZd01Gb1hEVEl6TURjeU5URTUKTlRZd01Gb3djVEVMTUFrR0ExVUVCaE1DV2tFeEVEQU9CZ05WQkFnVEIwZGhkWFJsYm1jeEZUQVRCZ05WQkFjVApERXB2YUdGdWJtVnpZblZ5WnpFVE1CRUdBMVVFQ2hNS1MzVmlaWEp1WlhSbGN6RVBNQTBHQTFVRUN4TUdSR1YyCmIzQnpNUk13RVFZRFZRUURFd3BMZFdKbGNtNWxkR1Z6TUlJQ0lqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FnOEEKTUlJQ0NnS0NBZ0VBdTU3TnlXVjVwTGxXcWZGSnRSYktQc1BDRVRtYzhtZElvemswOUFiUUZzeVdOTVFTTDFWVgpkR3FtMzVlQ2crMkNPOE5rOGc1Nk94YzhpbWE5dHNEa3VLRnFxdHkveWx3U1p2TUhLU0ZtTDFEY0VuelFXQS9JClRLeEtTelhlWTY1QlU3bENkNGVBYjhKK1YveEJhSWx1QWZzcUNDTzBPYU1vSmV1THpKUW1HODdCTlo2N0lUUWcKSGZWWHBoMXNaQXg1RHQzWmtyaS93MWtEays5aVdtcG1XRTFYcDB4RWIrYWRUT2lEZGFUV3U0ZmZiZHd0SE96dwo1RnRKK3pQUTRNbjBQT2g1MXpwSVpZY0FxVnZTaGR6cWNxa0hoZ0Q0V2FET00xWDRMUmZpVkRkQUszQkdoOHlNCmRkN3IrQUtsdUY4cEdqUTREQ0RMNzBoOG1LSE5MK3l0NmFBM1A4M2QzT3BFYzErU2dWYkRVWjkxYWo0MGY3L3EKOU9uSldyTzNtV2Z1WU9ja0d3WjYvakdUcWNVaVNHRDgwb2NTVXNJZE9PTUpiYVJ4aFlxVnJqQi9YM1pQbE02dwo5MzU1dnNHWWVUZHJUaG1UMzYxdENnRVN6TVVFRjBWNXNrVVJvYTJWaElrclBVa0V1akVHbGt2aG1COCtNYmlUCjRCZThXK0ZNTitCUFpaTitMTVNxWnh4NTZKeXFrVERFRGNMTVVFN3NEdXdYMXVWZTFUNjdISU5wVk1RRGlXWUsKY3ZoaWpCNy9yeEsvSmFCQ1FVQUs5MlBUSmd5MW9qRGpNQlFIZm9pWEZpSHc4MFNxWmVrNXRBc001YUFvN21XMgp0aUJWUjBzSlh0TTNRUlNqdTZiaW5HQ0tPRURvSU5JTXV0NzRjU1NGdWVIZ3k5K2pjdndoM0pVQ0F3RUFBYU5tCk1HUXdEZ1lEVlIwUEFRSC9CQVFEQWdFR01CSUdBMVVkRXdFQi93UUlNQVlCQWY4Q0FRSXdIUVlEVlIwT0JCWUUKRkNUV2xVdUNsS056NmE4YTFoOG45elRxaGdyMU1COEdBMVVkSXdRWU1CYUFGQ1RXbFV1Q2xLTno2YThhMWg4bgo5elRxaGdyMU1BMEdDU3FHU0liM0RRRUJEUVVBQTRJQ0FRQjJPSXRyUjBoN1RKM1ZtbXB1UmIzNzhaYktGVmRRCnNmK2lQeUhIdEd3ZmhPNFFMSnRNWU9OTDZ4cGQ4UlZidmhMdlFZL1ZnZ2pMTytXRE10NHBrTiswRkNNanpyVW8KM3psYUJsUk52Z0RsbU40L1RNLy9KQnVHQU5iRmZIRE5hb0xLS1ppdmNiZ2N4MnNMM3lTSS81TTNDKzRiRUhVYgpqL3Z1dDN3SUhGYldJVUQyUEVCVWFmd3YxbFk0QUNoaXVoWjhkeHkvVWFWYkFuQ2pmb2FoaGlDTER4cHNHS2x6Ci9USXJaNFJzVyszZUFmcVJ4dWx4SjdRUEd2dmJIRkREQjNITUk5Vk5tQnFnZjYyQnQ2Y0N6WjVzQUVkM2NnRWUKNzh3ZDNRTlNlQ0ZndmtDRHBKWFlmSlNib1ErMjNSY2NUZTBRTVlWOGhkT1NicTRhbkhaUllnMUtSOEszdnA3RgoxUkhmSjlNMlltY2dtS0FJUWk3RHgrMGVCV2Z3NXN3YnMrUGNGTURwbmhGZTRRQWRKWktEWGxSdGRxaWV5eHc0Ck1SbENISjVzUnFibUpXRFlvaGlXdnJuU1o0NVMwMHdxbUxRQjhlVEtFVHhxUTNTMVM5bWJVZDdrT3F5WmxUTnYKSlIxb1dzdWtTajBhUytjTms1c3o1bUJ0S2lCMGtKQjhJNFQ4UkFXcllkRVg5V09IdlJncGlrOExVNDNGU2RWYwptZkQ3TDErenZ3RXJOb1dtSkFhNmo1Q1BTdXBGallrcEtuOTh3cGNzV21MK3pDTEE3VHhzZE1ucW51RUZUVVc4CmxOTTJydDlsV0l1eWE2SXRrVnhKNlZPdEJQZk11cmtKM09nUUd6Y2U3VnJHNkdaZWx3UjdRdWhDamM0UUVPN20KeVpYc3Y0RnU4YXc0Qmc9PQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg=="
  etcd_cert: "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUdTVENDQkRHZ0F3SUJBZ0lVTElmVXZ5TWxESXhPQnlNS1Y1aWNSZTZ6STBJd0RRWUpLb1pJaHZjTkFRRU4KQlFBd2NURUxNQWtHQTFVRUJoTUNXa0V4RURBT0JnTlZCQWdUQjBkaGRYUmxibWN4RlRBVEJnTlZCQWNUREVwdgphR0Z1Ym1WelluVnlaekVUTUJFR0ExVUVDaE1LUzNWaVpYSnVaWFJsY3pFUE1BMEdBMVVFQ3hNR1JHVjJiM0J6Ck1STXdFUVlEVlFRREV3cExkV0psY201bGRHVnpNQjRYRFRFNE1EY3lOakU1TlRjd01Gb1hEVEk0TURjeU16RTUKTlRjd01Gb3djVEVMTUFrR0ExVUVCaE1DV2tFeEVEQU9CZ05WQkFnVEIwZGhkWFJsYm1jeEZUQVRCZ05WQkFjVApERXB2YUdGdWJtVnpZblZ5WnpFVE1CRUdBMVVFQ2hNS1MzVmlaWEp1WlhSbGN6RVBNQTBHQTFVRUN4TUdSR1YyCmIzQnpNUk13RVFZRFZRUURFd3ByZFdKbGNtNWxkR1Z6TUlJQ0lqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FnOEEKTUlJQ0NnS0NBZ0VBM1ZpbkovZ01DT0NNMER6TXRjdFFqOStiY0xuemNLdk9yWGVmby9HQTFKcGIrQkZsRmo0VQppMW82SGpjT3ROelYxZThUOXVqUXBMNDU5R01wak45NVExaGNMc3EzWHdham9qZ3d6UlBMWkFNVi9IU0d3Z3JHCjhkdWlQL0tTSnIydW42NmxmWkRFcW9hVGF5UmR3NFNQVXFxbkNoU0tyMko1eWgwRFg3L1NHN0NmL1F2MGZlcUIKb1BLTUZISGVWYmFqNktZa2JnTE5hM3JacTcwalcrazBUNkZGbGhNUHZKVHIrdGZESnFsdG5UZVFEM1hLUlpGbQpDdVppTFdRNzkzQkNUYlhjTkZUTWVoZFVaUFRSbjl5MnVScUpjM3NUaDVhWFYvQUdUYWtaZ0VIUHRMcWR1b0xVCk8rYUUzRDhqdnc3QTFGc1l1Q252UExTeUN5SjNGNWlWeW1uR0N0K3FnZFNMUWRrcHpESWYwWWhGQ1ZXb2JUS0oKUlRvcEFucEtUWllVaDJYTUQyYmlBdWxwMEZnOWhyU3pON3gzVWczTnA0Nk5RbU11dzNHN1JhWVJXQ3RQZ0lGdQpjQjJObllLWUl0Ti9VVEoxNk0wMEMwZndCNDZJWG9jaUxKV0dreEhmMGVTeXpFSmlTS2tXMEZXdVF2ZjVaNHpFClVaN2RuSEM2S2pmU3hyZTBGWEJ6RjZoMkFNa3prZEltOW8xVllHekpHRVNySFZ6K0pvbFp6TEUwK3ZQWW9pK0IKcFp3OGh0Y29xYkswTVZITjgvWTUzOXRSeXA4cGloWEVFWkswa3FKY0tXVU9zNUVuNnhSRWV0M1cwdHBwTWFHMwpNSXMxTGo2SHlBNUNHM1ptRTE2czh1dW9PRzRidVpMWXl1K2tYR21IZGdES29JUnJqaE1XSHBzQ0F3RUFBYU9CCjJEQ0IxVEFPQmdOVkhROEJBZjhFQkFNQ0JhQXdIUVlEVlIwbEJCWXdGQVlJS3dZQkJRVUhBd0VHQ0NzR0FRVUYKQndNQ01Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSME9CQllFRkRydTBrQWhUSnBDMmdSR3lmem5FMThKbk82eApNQjhHQTFVZEl3UVlNQmFBRkNUV2xVdUNsS056NmE4YTFoOG45elRxaGdyMU1GWUdBMVVkRVFSUE1FMkNFbXQxClltVnlibVYwWlhNdVpHVm1ZWFZzZElJTlkyeDFjM1JsY2k1c2IyTmhiSWNFTnE3TmNZY0VDaUFBQVljRXJLZ0EKQlljRXJLZ0JCWWNFTmxVakRvY0VFdHpxNTRjRWZ3QUFBVEFOQmdrcWhraUc5dzBCQVEwRkFBT0NBZ0VBY3k5VwpjbnNkZnFZZkhxVGlRWHhuVU1oQVhUamtwUnJrYjN1NXFmRDVBd0pKdTdTL3RtcW1qSFo3QlEvMFIwYnhWejJpCjNyVE94dTN1eEtwV0NSR2NyWGx5dU0xMGhxMG82TkJMQXorVGc1UHdWN0hZdStrdlFpNWZKTGVGL2FMM2pYa2MKMWxqNFpIRVFPdlZDTTlFZ0UyeFgxV2xySVMyVkY1bnZWb2hPRlpzdUFYUWllbzhrOERoSXdXV1pmWFRaWFp3VgpRL29qZEFSais3dC9IcjRQZElrN2NOdndCSitaOTV2bmUyY0hTMDc0b1dZN1VpV0FZKy9JaklsQ1Z6OWprZysxCmNRSWhReVlJM0YyWTBlbnNpWmQrbDdTOVh5NDVUdzJpTW9oRVpLa2RMNHozNkg4d05BZlBYUXZTNWpZSFlqaUsKZnN5RUc1M3JBVk9SRndac05XaUdTdHBUYUs1S0ZFL05lbFR6RVpsSnRZL2h5ZFhiWSt6bUYxWmU2VDVPMUlEcwpGWWpPelRETEhsbmJ0MnRXMy9RbXN0K0xCN2dPK3haNmV3VU5DU0NrVFQycWlmUTZCSC84dlVHZzhPSzRrNXpyClZ2b0p3Z3RVUEZpL3FOMDErNDFDZVMrckFZTHhodGQ0UTlGaDlnRE1QdEJSSGkvSWFxQVBuemN0QzJubjNUR0UKTEJIeGR3RTdiZGM4Uk5NMGFkWWpqWTkvRys3ZFdCRGRqYWorOUtTTzRYZmtxK2hVMERYMWJtOGtPODBUT0tRUQpDNmk4V3U4VFdhUmdnT2MvN3lYVTl1bk9OdGJZamJGZVlHUnd5Mkt4ejRKRjdHLzB6TzFzVjhjZDJCRWFWaURRCkJBR3JxcFl2SDdha1ZtdWhmRWNlbmZqQW1uOVZqdmlFYXVKeW9oRT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo="
  etcd_key: "LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlKS1FJQkFBS0NBZ0VBM1ZpbkovZ01DT0NNMER6TXRjdFFqOStiY0xuemNLdk9yWGVmby9HQTFKcGIrQkZsCkZqNFVpMW82SGpjT3ROelYxZThUOXVqUXBMNDU5R01wak45NVExaGNMc3EzWHdham9qZ3d6UlBMWkFNVi9IU0cKd2dyRzhkdWlQL0tTSnIydW42NmxmWkRFcW9hVGF5UmR3NFNQVXFxbkNoU0tyMko1eWgwRFg3L1NHN0NmL1F2MApmZXFCb1BLTUZISGVWYmFqNktZa2JnTE5hM3JacTcwalcrazBUNkZGbGhNUHZKVHIrdGZESnFsdG5UZVFEM1hLClJaRm1DdVppTFdRNzkzQkNUYlhjTkZUTWVoZFVaUFRSbjl5MnVScUpjM3NUaDVhWFYvQUdUYWtaZ0VIUHRMcWQKdW9MVU8rYUUzRDhqdnc3QTFGc1l1Q252UExTeUN5SjNGNWlWeW1uR0N0K3FnZFNMUWRrcHpESWYwWWhGQ1ZXbwpiVEtKUlRvcEFucEtUWllVaDJYTUQyYmlBdWxwMEZnOWhyU3pON3gzVWczTnA0Nk5RbU11dzNHN1JhWVJXQ3RQCmdJRnVjQjJObllLWUl0Ti9VVEoxNk0wMEMwZndCNDZJWG9jaUxKV0dreEhmMGVTeXpFSmlTS2tXMEZXdVF2ZjUKWjR6RVVaN2RuSEM2S2pmU3hyZTBGWEJ6RjZoMkFNa3prZEltOW8xVllHekpHRVNySFZ6K0pvbFp6TEUwK3ZQWQpvaStCcFp3OGh0Y29xYkswTVZITjgvWTUzOXRSeXA4cGloWEVFWkswa3FKY0tXVU9zNUVuNnhSRWV0M1cwdHBwCk1hRzNNSXMxTGo2SHlBNUNHM1ptRTE2czh1dW9PRzRidVpMWXl1K2tYR21IZGdES29JUnJqaE1XSHBzQ0F3RUEKQVFLQ0FnRUFrclZsNVpvRThBVlZqOWdjbndvcFZaTVNCZjlhR1gzbXVSY21tcFZCbktqSytIc2ZFMFVHZk83Swp3cWdmZVVVOS9sUStXVk1xcDJibUp0TnJBTkpHSDVyN1RwQlNSZ3V5Zlp6Y3k3Q3BDSnI3QVorQ2lpNTJlbXZtCitKZWd3MGpKNjFEb28zZEd3dmY3MGZJQ3E4cTY3RUhzQVVSb1A2TDNzanpVdGF3QWFSWUNsTWhOdnYrbDk3V0wKbnlseUFrSFNGaXU0emFuUjJjRk5jUVVsNEkrTGVwUWIrVUtkRlpObmJaV0E1YWs2M0ErdHpuRjJKZklmUG01QQorSEVOUmc0RUJGbmJvRnZzUTZnZTZlTlM1YkIyOCsxZzJVZmtRK1JPWDV6WnNMTzZFVWFOa3U1MkNOOGFLTEc3CnNmZXZpNzBObGpMakNYM1I4ZHlhZmNMVVNMRjc1YmRiRkNrSWsvQnNGTkZQZnV4Y2kvdFhlT09vUW13UzRjcFMKN3ptNHc5TUhJNlY5YWhIRGtIMFhsa204SkcrSW1FcWJZUnQ5SE1iRWhXdCsyUFQyRmhielhuL3UyamdGVDFvRgp2amltdkRPVWNXcTdLTVhNSnRiZGwrcVlraTdwOWtuSVF0Rm52SHNVZ3R6TTJaTlpXUVJEeG00bng0TWt5ZkROCmxldVlLTUtxS3Y2T1ZveU1rMU9VVEp2cW1jQ3l5QWY1cVd6L1kyR2k3Wnk0WFUrMWNyZTVCQktLZFZJSFpzMzkKenpxNUZJcjRhMlU2YkpqZk1CRTlzVkpOT1BuUnpXMWw5azlPRldiMjlIRlVHeGUzTE9CbjhTOXBXMDV5UjI2agpyNTMydHFFYnVzdGt3OUxlb3YrRmNCQ1VaWDZmV1NOeWNRN3ZZYlplVGd0aHhYRHdNdUVDZ2dFQkFOL2ErYU1zClB3cjBSSVVQeEkwWC9Cek5PeVQ2Wk9yVk5teG55NERFV2JNK1hhMzZKY3IyRXlDbnF2VnE0L3FtTE5qOEJuNHIKaHduckdBVjhieFk5TGJaSHZHckw5bDNVN3RGZGx0QWp0ejBaQVZBT1pVVDlXYXorM1h3S3M0dWhhZmZCRG93cAorbVBzWjJGL2doMkcyQm1rQXVMZjA1MSs4L2JEUmhvQVVCR2d0N21NWk9zS0xiK3NRdlNaaUcydlJOMkc4Y2NaCjIzRlBrSmk1VFJweVAzQitIVGVDRHZTQ3laYStsWnp4dWVZZi8yVkRWUVp3dUc2VFV3Um1xcUxDMTViMjdma3AKbHRUV3oyRCs2cit5Z014WllYTVd2TXJOUlJxYnJSUkZTMFVEZjNQTVdVVFlyekUrMkVvL1p1Q0ZsZzVZNHVzRAp1cTZZdnlxc04zV0Jhb01DZ2dFQkFQMGhjWGJKZEticG5zMFJwZUtEcHpjN1NrQ245aXhsclBjWlZoeWk4MytTCnd1dXdIRG9YcHlBMGZwcnRESk0rQ3JGam9QUEFCRjhrdDF4cEJpYi8wR2hWNDBGcldob3NRWCs4czVqV0R0dWsKMi8wUUhlTk1KMnZXeDliR2g2OEVUcEJYMERWQzZJUzl4elYrZDNvMDQ5aHZtaFowUkxLVUQ0a1BGcG1YdWhpQgpaZmJUMTdzZ3dVUmk2VSttWkMzdEpYb1lZQVFJc3JOVXE3V2JMVERMZGdhU0w3MW9YTVdrQ1U5emcvV2w5a1VWCkowdFZPaWFockpFMjBmQ0NBSDVjMzh0ckJNYVhmK3dNVG8wejdOOEhqMEg5QStJbWZLck1PcXFEa1UydGJUZDIKUG9XQ01XOEx2QXhOOWl6MUdVUEZSUnNmdC9Uc1gwUkpRMWxNOUFtVElBa0NnZ0VBUDZKNThINjFkU0czRUxSSQp4V3NhQ2YzMVlDK2wzSE83d3RsR3FsWW13YU5xOGZmNElsZ05XKzBOMWpJS21NZ0FXN1paQ2lqSmZISk9JOUVaCnF4SzlHZUlPbDdWZlZaRWdTbmYyVlRpeDhEWlpmeTU0YjBJdDZkV1JsSjllSnNVOTRHWUxBcUUwUW8rRFc5M3AKV1FkcUN1L3F1aCt5Z2tVQ2lSVTlxa3FJZUZwZG4rdGZETW1aODdDK1g5WDlTR0RZakxpOXBrR2RkZnFPT2p3Qwp4US9SSk9OVTN1Q2o2N0g2S0ZZSkpkSTViRWVRVEhMMWlwMGtjSnFFc08yV2VDbzNLdjBLN1JkN2Z3bUpiaU5KCkRERnNGNnNZM3FTS0NvU0xybUt1c3VKMzRTaDdaS21rZnNPeUdhZXB5SUY1ZHozQ1NGWFFPOHFWbU9obS8zUngKVjMvd2pRS0NBUUJaY3hMOGJnNDUyWDZUYXZRYXlua3hyRkw5N0J5eTdhOGtscUIyZUNMaDV3UFljNkJWNnZ2VgpCcGZpdVhmakpGNTA5d2J1ZStGUG1kTHQ1Zk1WbnZxUGxDSTlnT2tUTEtac3RFc203Q1hOK2tEKytVNEdmMmZDCjFPQkRobDUyTzJoajI1UW83aDdxb1lHZTVyVGp5NExrZmZPT2tjR2c5VzlUK0svODBNQWpYcjZnYlpYamU1eXcKUk4rN2xNbjc3THg4bTlaNk5uU25xSGtSTU9mVVFXSFF5cUh5SmNuNjBZKzBzdFpqM1NUY25yQUhEL1AvcU13cAplSnpSaDY4UytGRGhPazB5dkdDcEVtUkR3NkNsNDY3OUc2TGVyTWtMTmlaWkVTc3NydWRVWFFXVStqL3J4TkxMCjZsVGU2REhvWC9LZXRpTzBlajdrQzV1d0YrQjQ3Yk81QW9JQkFRREtOeEJ0S1E4SE1lSkd0WTBtSWRpMFkrcEEKWFlDSjlvN0xvNXVId3cvdlFKVm9kdmU4YzVmMzI3T0ZCdTBQL08rbkhBRWFXU2ptZ25GRXRUa2VPcU83VVM3Mwp5VXh5VEhPb2dSd28weUdUMGEvOFpuS2h1SmVOd3hCSUpKbzJtLyttMDFkTDNlbDFDMVJuMWo3QUx3V2xuMTN2CjdwK0FRVUhMVWZuM3IwYTh4ZzBkek9aVmc5TVlNdkszYUNWNGdHZGJlUFRXMHhqYlhLK2hrVko2NlVkRzFPNTEKRkdwQ01EQ0FaYzJwZDd3VmpHcUxDTGFzZER1emVYRko2RmczKzEvRTJ1Qk5kMUo3am1IaWNVblR1YWtGSWNveQpmaElWUVVPZ2tLdHF0TEJWeXV1VGZadXEwOUpIN0VNNTBQM2ZyRFhPQVJXbjVhQldYbGQyV29NOXphclEKLS0tLS1FTkQgUlNBIFBSSVZBVEUgS0VZLS0tLS0K"

---

# This manifest installs the per-node agents, as well
# as the CNI plugins and network config on
# each master and worker node in a Kubernetes cluster.
kind: DaemonSet
apiVersion: extensions/v1beta1
metadata:
  name: canal-node
  namespace: kube-system
  labels:
    k8s-app: canal-node
spec:
  selector:
    matchLabels:
      k8s-app: canal-node
  updateStrategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
  template:
    metadata:
      annotations:
        scheduler.alpha.kubernetes.io/critical-pod: ''
      labels:
        k8s-app: canal-node
    spec:
      tolerations:
        # Make sure canal node can be scheduled on all nodes.
        - effect: NoSchedule
          operator: Exists
        # Mark the pod as a critical add-on for rescheduling.
        - key: CriticalAddonsOnly
          operator: Exists
        - effect: NoExecute
          operator: Exists
      hostNetwork: true
      serviceAccountName: canal
      # Minimize downtime during a rolling upgrade or deletion; tell Kubernetes to do a "force
      # deletion": https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods.
      terminationGracePeriodSeconds: 5
      containers:
        # Runs calico/node container on each Kubernetes node.  This
        # container programs network policy and local routes on each
        # host.
        - name: calico-node
          image: quay.io/calico/node:v3.1.3
          env:
            # The location of the etcd cluster.
            - name: ETCD_ENDPOINTS
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_endpoints
            # Location of the CA certificate for etcd.
            - name: ETCD_CA_CERT_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_ca
            # Location of the client key for etcd.
            - name: ETCD_KEY_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_key
            # Location of the client certificate for etcd.
            - name: ETCD_CERT_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_cert
            # Disable Calico BGP.  Calico is simply enforcing policy.
            - name: CALICO_NETWORKING_BACKEND
              value: "none"
            # Cluster type to identify the deployment type
            - name: CLUSTER_TYPE
              value: "k8s,canal"
            # Disable file logging so `kubectl logs` works.
            - name: CALICO_DISABLE_FILE_LOGGING
              value: "true"
            # Set noderef for node controller.
            - name: CALICO_K8S_NODE_REF
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: FELIX_HEALTHENABLED
              value: "true"
          securityContext:
            privileged: true
          resources:
            requests:
              cpu: 250m
          livenessProbe:
            httpGet:
              path: /liveness
              port: 9099
            periodSeconds: 10
            initialDelaySeconds: 10
            failureThreshold: 6
          readinessProbe:
            httpGet:
              path: /readiness
              port: 9099
            periodSeconds: 10
          volumeMounts:
            - mountPath: /lib/modules
              name: lib-modules
              readOnly: true
            - mountPath: /var/run/calico
              name: var-run-calico
              readOnly: false
            - mountPath: /calico-secrets
              name: etcd-certs
            - mountPath: /etc/etcd
              name: calico-node-etcd-certs
              readOnly: true
        # This container installs the Calico CNI binaries
        # and CNI network config file on each node.
        - name: install-calico-cni
          image: quay.io/calico/cni:v3.1.3
          imagePullPolicy: Always
          command: ["/install-cni.sh"]
          env:
            # The name of the CNI network config file to install.
            - name: CNI_CONF_NAME
              value: "10-canal.conflist"
            # The location of the etcd cluster.
            - name: ETCD_ENDPOINTS
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_endpoints
            # Location of the CA certificate for etcd.
            - name: ETCD_CA_CERT_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_ca
            # Location of the client key for etcd.
            - name: ETCD_KEY_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_key
            # Location of the client certificate for etcd.
            - name: ETCD_CERT_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_cert
            # The CNI network config to install on each node.
            - name: CNI_NETWORK_CONFIG
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: cni_network_config
          volumeMounts:
            - mountPath: /host/opt/cni/bin
              name: cni-bin-dir
            - mountPath: /host/etc/cni/net.d
              name: cni-net-dir
            - mountPath: /calico-secrets
              name: etcd-certs
      volumes:
        # Used by calico/node.
        - name: lib-modules
          hostPath:
            path: /lib/modules
        - name: var-run-calico
          hostPath:
            path: /var/run/calico
        - name: calico-node-etcd-certs
          hostPath:
            path: /etc/etcd
        # Used to install CNI.
        - name: cni-bin-dir
          hostPath:
            path: /opt/cni/bin
        - name: cni-net-dir
          hostPath:
            path: /etc/cni/net.d
        # Mount in the etcd TLS secrets.
        - name: etcd-certs
          secret:
            secretName: calico-etcd-secrets

---

# This manifest deploys the Calico policy controller on Kubernetes.
# See https://github.com/projectcalico/k8s-policy
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: calico-kube-controllers
  namespace: kube-system
  labels:
    k8s-app: calico-kube-controllers
spec:
  # The policy controller can only have a single active instance.
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      name: calico-kube-controllers
      namespace: kube-system
      labels:
        k8s-app: calico-kube-controllers
      annotations:
        scheduler.alpha.kubernetes.io/critical-pod: ''
    spec:
      # The policy controller must run in the host network namespace so that
      # it isn't governed by policy that would prevent it from working.
      hostNetwork: true
      serviceAccountName: calico-kube-controllers
      tolerations:
        # Make sure canal node can be scheduled on all nodes.
        - effect: NoSchedule
          operator: Exists
        # Mark the pod as a critical add-on for rescheduling.
        - key: CriticalAddonsOnly
          operator: Exists
        - effect: NoExecute
          operator: Exists
      containers:
        - name: calico-kube-controllers
          image: quay.io/calico/kube-controllers:v3.1.3
          env:
            # The location of the Calico etcd cluster.
            - name: ETCD_ENDPOINTS
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_endpoints
            # Location of the CA certificate for etcd.
            - name: ETCD_CA_CERT_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_ca
            # Location of the client key for etcd.
            - name: ETCD_KEY_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_key
            # Location of the client certificate for etcd.
            - name: ETCD_CERT_FILE
              valueFrom:
                configMapKeyRef:
                  name: canal-config
                  key: etcd_cert
            # Choose which controllers to run.
            - name: ENABLED_CONTROLLERS
              value: policy,profile,workloadendpoint,node
          volumeMounts:
            # Mount in the etcd TLS secrets.
            - mountPath: /calico-secrets
              name: etcd-certs
            - mountPath: /etc/etcd
              name: calico-controller-etcd-certs
              readOnly: true
      volumes:
        # Mount in the etcd TLS secrets.
        - name: etcd-certs
          secret:
            secretName: calico-etcd-secrets
        - name: calico-controller-etcd-certs
          hostPath:
            path: /etc/etcd
---

apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: canal
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: canal
subjects:
- kind: ServiceAccount
  name: canal
  namespace: kube-system

---

kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: canal
rules:
  - apiGroups: [""]
    resources:
      - pods
      - nodes
    verbs:
      - get

---

apiVersion: v1
kind: ServiceAccount
metadata:
  name: canal
  namespace: kube-system

---

apiVersion: v1
kind: ServiceAccount
metadata:
  name: calico-kube-controllers
  namespace: kube-system
