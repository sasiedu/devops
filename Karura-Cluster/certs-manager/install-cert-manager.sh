# Instal cert-manager [https://cert-manager.readthedocs.io] with helm 
#!/bin/bash

helm install \
    --name cert-manager \
    --namespace kube-system \
    stable/cert-manager