#!/bin/bash

kubectl apply -f rbac.yaml
kubectl apply -f configMap.yaml
kubectl apply -f pvc.yaml
kubectl apply -f server.yaml
kubectl apply -f agent.yaml
kubectl apply -f ingress.yaml