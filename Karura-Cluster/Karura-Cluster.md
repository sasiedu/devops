# KARURA KUBERNETES CLUSTER

## Cluster Details
* Cluster VM CIDR: 172.168.0.0/16
* DHCP domain-name-servers: 8.8.8.8, 172.16.16.16, 208.67.222.222, 208.67.220.220
* DHCP NTP-servers: 216.239.35.0, 216.239.35.4, 216.239.35.8, 216.239.35.12

## North Virginia VPC Details
* VPC CIDR: 172.168.0.0/24
* VPC Name: Karura
* Subnet: 172.168.0.0/24
* Subnet Name: Karura-subnet
* DHCP domain-name: karura.north.virginia

## North Virginia Node Details
* Karura-worker-0-north-virginia
  * Private IP: 172.168.0.5
  * Hostname: ip-172-168-0-5.karura.north.virginia
  * Public IP: 54.85.35.14
  
## Ohio VPC Details
* VPC CIDR: 172.168.1.0/24
* VPC Name: Karura
* Subnet: 172.168.1.0/24
* Subnet Name: Karura-subnet
* DHCP domain-name: karura.ohio

## Ohio Node Details
* Karura-worker-0-ohio
  * Private IP: 172.168.1.5
  * Hostname: ip-172-168-1-5.karura.ohio
  * Public IP: 18.220.234.231

## HAProxy Server Details
* Public IP: 54.174.205.113
* Username: sasiedu
* Passwd: sasiedu
* Stat Uri: /haproxy?stats

## Modprobe
* modprobe dm_thin_pool
* modprobe br_netfilter

### NB
* Remove all ip links but eth0 and lo when changing cluster networking
* Clear the following folders when changing cluster networking: /var/lib/cni/, /etc/cni/net.d/, /var/lib/kubelet/
