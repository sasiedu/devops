---
- hosts: kube-node
  remote_user: ubuntu
  become: true
  tasks:
  - name: Create folders if not exists
    file:
      path: "{{ item }}"
      state: directory
    with_items:
    - "/etc/cni/net.d/"
    - "/opt/cni/bin/"
    - "/run/flannel"
  - name: Download cni plugins tar and flanneld
    get_url:
      url: "{{ item.url }}"
      dest: "{{ item.dest }}"
    with_items:
    - { url: "https://github.com/containernetworking/plugins/releases/download/v0.7.1/cni-plugins-amd64-v0.7.1.tgz", dest: "/tmp"}
    - { url: "https://github.com/coreos/flannel/releases/download/v0.10.0/flannel-v0.10.0-linux-amd64.tar.gz", dest: "/tmp"}
  - name: Extract and copy cni plugins and flanneld
    shell: tar -xf {{ item.tar }} -C {{ item.dest }}
    args:
      chdir: "{{ item.chdir }}"
    with_items:
    - { tar: "cni-plugins-amd64-v0.7.1.tgz", dest: "/opt/cni/bin/", chdir: "/tmp"}
    - { tar: "flannel-v0.10.0-linux-amd64.tar.gz", dest: "/usr/local/bin/", chdir: "/tmp"}
  - name: Generate flannel network config
    template:
      src: templates/flannel-network-config.json.jinja2
      dest: "{{ K8S_PATH.CONFIG }}/flannel-network-config.json"
  - name: Write flannel network to etcd
    shell: |
      etcdctl \
        --endpoints=https://{{ ETCD_SERVERS[0].IP }}:2379 \
        --ca-file={{ K8S_PATH.CERT }}/ca/ca.pem \
        --cert-file={{ K8S_PATH.CERT }}/api/kubernetes.pem \
        --key-file={{ K8S_PATH.CERT }}/api/kubernetes-key.pem \
        set /coreos.com/network/config < {{ K8S_PATH.CONFIG }}/flannel-network-config.json
  - name: Generate flanneld service file
    template:
      src: templates/flanneld.service.jinja2
      dest: /etc/systemd/system/flanneld.service
  - name: Reload systemctl daemon
    shell: systemctl daemon-reload
  - name: Enable and restart flanneld service
    service:
      name: flanneld
      enabled: yes
      state: restarted
