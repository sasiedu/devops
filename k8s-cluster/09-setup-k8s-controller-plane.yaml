---
- hosts: kube-master
  remote_user: ubuntu
  become: true
  tasks:
  - name: Create folders if not exists
    file:
      path: "{{ item }}"
      state: directory
    with_items:
    - /var/lib/kubernetes
    - /etc/kubernetes/config
  - name: Download k8s controller binaries
    get_url:
      url: "{{ item.url }}"
      dest: "/usr/local/bin/{{ item.name }}"
      mode: 0755
    with_items:
    - { url: "https://storage.googleapis.com/kubernetes-release/release/v1.11.0/bin/linux/amd64/kube-apiserver", name: "kube-apiserver" }
    - { url: "https://storage.googleapis.com/kubernetes-release/release/v1.11.0/bin/linux/amd64/kube-controller-manager", name: "kube-controller-manager" }
    - { url: "https://storage.googleapis.com/kubernetes-release/release/v1.11.0/bin/linux/amd64/kube-scheduler", name: "kube-scheduler" }
    - { url: "https://storage.googleapis.com/kubernetes-release/release/v1.11.0/bin/linux/amd64/kubectl", name: "kubectl" }
  - name: Copy certs and configs
    copy:
      src: "{{ item.src }}"
      dest: "{{ item.dest }}"
      remote_src: yes
    with_items:
    - { src: "{{ K8S_PATH.CERT }}/ca/ca.pem", dest: "/var/lib/kubernetes/ca.pem" }
    - { src: "{{ K8S_PATH.CERT }}/ca/ca-key.pem", dest: "/var/lib/kubernetes/ca-key.pem" }
    - { src: "{{ K8S_PATH.CERT }}/api/kubernetes.pem", dest: "/var/lib/kubernetes/kubernetes.pem" }
    - { src: "{{ K8S_PATH.CERT }}/api/kubernetes-key.pem", dest: "/var/lib/kubernetes/kubernetes-key.pem" }
    - { src: "{{ K8S_PATH.CERT }}/service-account/service-account.pem", dest: "/var/lib/kubernetes/service-account.pem" }
    - { src: "{{ K8S_PATH.CERT }}/service-account/service-account-key.pem", dest: "/var/lib/kubernetes/service-account-key.pem" }
    - { src: "{{ K8S_PATH.CONFIG }}/encryption-config.yaml", dest: "/var/lib/kubernetes/encryption-config.yaml" }
    - { src: "{{ K8S_PATH.CONFIG }}/controller/kube-controller-manager.kubeconfig", dest: "/var/lib/kubernetes/kube-controller-manager.kubeconfig" }
    - { src: "{{ K8S_PATH.CONFIG }}/scheduler/kube-scheduler.kubeconfig", dest: "/var/lib/kubernetes/kube-scheduler.kubeconfig" }
  - name: Get private ip
    shell: hostname -I | sed s/\ //g
    register: private_ip
  - name: Generate systemd service files
    template:
      src: "templates/{{ item }}.jinja2"
      dest: "/etc/systemd/system/{{ item }}"
    with_items:
    - "kube-apiserver.service"
    - "kube-controller-manager.service"
    - "kube-scheduler.service"
  - name: Reload systemctl daemon
    shell: systemctl daemon-reload
  - name: Enable and restart services
    service:
      name: "{{ item }}"
      enabled: yes
      state: restarted
    with_items:
    - "kube-apiserver"
    - "kube-controller-manager"
    - "kube-scheduler"