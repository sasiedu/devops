# K8S-CLUSTER

## Setup /etc/hosts
```text
Add contents of etc.hosts file to /etc/hosts file of all vms/servers
```

## Public ip connection ansible playbooks
```bash 
ansible-playbook -v -i hosts.ini { playbook name }
```
Playbooks with public ip connection: 01, 02

## Private ip connection ansible playbooks
```bash
C_ARGS="-o ProxyCommand='ssh -i { ipsec server pem} -W %h:%p { ipsec server username }@{ ipsec server dns/ip }'"
ansible-playbook -v -i hosts.ini --ssh-common-args="`echo $C_ARGS`" { playbook name}
```