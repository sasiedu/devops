## DEVOPS ANSIBLE

### PRE-INSTALL
* Install python on remote servers
* Pip install ansible and jinja on host system

### Setup strongswan on ipsec servers
Add below to /etc/hosts of host system
``` bash
34.225.32.231 devops-north-virginia-0
18.228.50.203 devops-sao-paulo-0
```
Run command on host system
``` bash
ansible-playbook -i hosts.ini -e @vars.yaml vpn_ipsec/vpn.yaml

for verbose
ansible-playbook -vvv -i hosts.ini -e @vars.yaml vpn_ipsec/vpn.yaml
```

### Add ssh keys or add remote-host (server)
Run command on host system
``` bash
ansible-playbook -i hosts.ini -e @vars.yaml vpn_ipsec/add_servers.yaml
```

### Setup etcd cluster
Run command on host system
``` bash
ansible-playbook -vv -i hosts.ini --ssh-common-args="-o ProxyCommand='ssh -i ./ssh/north-virginia.pem -W %h:%p ubuntu@devops-north-virginia-0'" --private-key=./ssh/north-virginia.pem -e @vars.yaml etcd/etcd.yaml
```

### Setup api-server
Run command on host system
``` bash
ansible-playbook -vv -i hosts.ini --ssh-common-args="-o ProxyCommand='ssh -i ./ssh/north-virginia.pem -W %h:%p ubuntu@devops-north-virginia-0'" --private-key=./ssh/north-virginia.pem -e @vars.yaml kubernetes/api-server.yaml
```