## DEVOPS CLUSTER WITH KUBERNETES
Setup up virtual machines using ansible

### DHCP CONFIG
```
(AWS)
name: devops
domain-name: {{vpc-data-center-location}}.devops
domain-name-servers = 8.8.8.8, 8.8.4.4
ntp-servers = 216.239.35.0, 216.239.35.4, 216.239.35.8, 216.239.35.12
```

### VM TAGS
* vpc:
* subnet:
* ipsec-server: true/false
* node-role: master/worker

## VPCS (PRIVATE NETWORKS)
* Network CIDR: 10.0.0.0/8
* VPC Name: Devops-cluster
* DHCP Options set: devops (AWS)
* K8s cluster service cidr: 192.168.0.0/16
* K8s cluster pods cidr: 175.20.0.0/16

### NORTH-VIRGINIA VPC (AWS)
* CIDR: 10.0.0.0/16
* Public Subnet: 10.0.0.0/24
* Private Subnet: 10.0.1.0/24
* IPSEC server Private IP: 10.0.0.5
* IPSEC server Public IP: 34.225.32.231

### SAO-PAULO VPC (AWS)
* CIDR: 10.1.0.0/16
* Public Subnet: 10.1.0.0/24
* Private Subnet: 10.1.1.0/24
* IPSEC server Private IP: 10.1.0.5
* IPSEC server Public IP: 18.228.50.203

## NODES
### ip-10-0-0-5.northvirginia.devops
* K8s pod cidr: 175.20.0.0/24

### ip-10-1-0-5.saopaulo.devops
* K8s pod cidr: 175.20.1.0/24

## K8S CLUSTER
### etcd servers
* 10.0.0.5
* 10.1.0.5

### api-servers
* devops.sasiedu.net
* 10.0.0.5
* 10.1.0.5
* 192.168.0.1 (k8s cluster service ip)
* 192.168.1.1 (k8s cluster service ip)
